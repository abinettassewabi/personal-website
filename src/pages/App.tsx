import React from 'react';
import logo from '../public/logo.svg';
import '../styles/App.css';
import Hero from '../components/Hero';

function App() {
  return (
    <div className="App">
      <Hero />
    </div>
  );
}

export default App;
