function SocialMedia(){
    return (
        <h1>
            Social Media section
        </h1>
    )
}

function CopyRight(){
    return (
        <h1>
            Copy Right section
        </h1>
    )
}

export default function Footer(){
    return (
        <>
            <SocialMedia />
            <CopyRight />
        </>
    )
}