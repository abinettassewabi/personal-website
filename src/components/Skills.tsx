function Certifications(){
    return(
        <>
            Certifications Section
        </>
    )
}

function Skill(){
    return(
        <>
            Skill Section
        </>
    )
}

export default function Skills(){
    return (
        <>
            <div>
                <div> <Skill /> </div>
                <div> <Certifications /> </div>
            </div>
        </>
    )
}