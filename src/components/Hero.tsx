import { styled } from "styled-components"

const Hero_section = styled.section`
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    justify-content: center;
    padding: 2rem;

    @media screen and (min-width: 768px) {
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
    }
`

const GreetingText = styled.div`
  color: white;
  max-width: 50%;
`;

const HeroImage = styled.div`
  margin-left: auto;

  @media screen and (min-width: 768px) {
    margin-left: 2rem;
  }
`;

function Introduction(){
    return (
        <GreetingText>
            <h1>
                Introduction section
            </h1>
            <p>i am <span>Abinet Tassew</span></p>
        </GreetingText>
    )
}

function Side(){
    return (
        <HeroImage>
            <img src="" alt=" " />
        </HeroImage>
    )
}

export default function Hero(){
    return (
        <Hero_section>
            <Introduction />
            <Side />
        </Hero_section>
    )
}
